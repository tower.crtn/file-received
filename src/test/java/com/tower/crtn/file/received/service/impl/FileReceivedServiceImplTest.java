package com.tower.crtn.file.received.service.impl;

import com.tower.crtn.file.received.component.CosManagementFile;
import com.tower.crtn.file.received.component.KafkaProducer;
import com.tower.crtn.file.received.constant.Constants;
import com.tower.crtn.file.received.mongo.entity.FileMongo;
import com.tower.crtn.file.received.mongo.repository.FileMongoRepository;
import com.tower.crtn.file.received.properties.KafkaProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

@SpringBootTest
@DirtiesContext
public class FileReceivedServiceImplTest {
  
  @InjectMocks
  private FileReceivedServiceImpl fileReceivedServiceImpl;
  
  @Mock
  private CosManagementFile cosManagementFile;
  
  @Mock
  private FileMongoRepository fileMongoRepository;
  
  @Mock
  private KafkaProducer kafkaProducer;

  @Mock
  private KafkaProperties kafkaProperties;
  
  private MultipartFile multipartFile;
  
  private String name;
  
  
  @BeforeEach
  public void setUp() throws FileNotFoundException, IOException {
    MockitoAnnotations.openMocks(this);
   
    name = "010101010101.txt";
    
    multipartFile = new MockMultipartFile(name, 
        new FileInputStream(new File(name)));
  }
  
  @Test
  public void receivedFileTest() {
    
    Mockito.when(fileMongoRepository.save(Mockito.any()))
    .thenReturn(new FileMongo());
    
    Mockito.when(kafkaProperties.getTopicFileReciver()).thenReturn(Constants.FILE_STRING);
    
    Mockito.doNothing().when(kafkaProducer).send(Mockito.anyString(), Mockito.anyString());
    
    fileReceivedServiceImpl.receivedFile(multipartFile, name);
    
  }
  
  
}
