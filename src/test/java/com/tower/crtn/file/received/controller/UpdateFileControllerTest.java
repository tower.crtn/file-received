package com.tower.crtn.file.received.controller;

import com.tower.crtn.file.received.constant.Constants;
import com.tower.crtn.file.received.controller.UploadFileController;
import com.tower.crtn.file.received.mongo.entity.FileMongo;
import com.tower.crtn.file.received.properties.ApiProperties;
import com.tower.crtn.file.received.service.FileReceivedService;
import lombok.EqualsAndHashCode.Include;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.multipart.MultipartFile;

@SpringBootTest
public class UpdateFileControllerTest {
  
  @InjectMocks
  private UploadFileController uploadFileController;
  
  @Mock
  private ApiProperties apiProperties;
  
  @Mock
  private FileReceivedService fileReceivedService;
  
  private MultipartFile multipartFile;
  
  private String name;
  
  private FileMongo fileMongo;
  
  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
    
    multipartFile = Mockito.mock(MultipartFile.class);
    name = "010101010101.txt";
    
    fileMongo = new FileMongo();
    fileMongo.setFileName(Constants.FILE_STRING);
    fileMongo.setPathCos("");
  }
  
  @Test
  public void uploadFileTest() {
    Mockito.when(fileReceivedService.receivedFile(
        Mockito.any(), Mockito.anyString())).thenReturn(true);
    Mockito.when(apiProperties.getResponseOk())
    .thenReturn(Constants.FILE_STRING);
    uploadFileController.uploadFile(multipartFile, name);
  }
  
  @Test
  public void uploadFileTestBad() {
    Mockito.when(fileReceivedService.receivedFile(
        Mockito.any(), Mockito.anyString())).thenReturn(false);
    Mockito.when(apiProperties.getResponseBad())
    .thenReturn(Constants.FILE_STRING);
    uploadFileController.uploadFile(multipartFile, name);
  }
  
  
  
  
}
