package com.tower.crtn.file.received.component;

import com.tower.crtn.file.received.constant.Constants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.util.concurrent.CompletableToListenableFutureAdapter;
import org.springframework.util.concurrent.ListenableFuture;
import java.util.concurrent.CompletableFuture;


@SpringBootTest
public class KafkaProducerTest {
  
  @InjectMocks
  private KafkaProducer KafkaProducer;
  
  @Mock
  private KafkaTemplate<String, String> kafkaTemplate;
  
  private CompletableToListenableFutureAdapter<String> completableToListenableFutureAdapter;
  
  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
    completableToListenableFutureAdapter = new CompletableToListenableFutureAdapter<>(new CompletableFuture<>());
  }
  
  @Test
  public void kafkaProducerContructorTest() {
    Assertions.assertNotNull(new KafkaProducer(kafkaTemplate));
  }
  
  @Test
  public void sendTest() {
    Mockito.when(kafkaTemplate.send(Mockito.anyString(), Mockito.anyString()))
    .thenReturn((ListenableFuture) completableToListenableFutureAdapter);
    
    KafkaProducer.send(Constants.FILE_STRING, Constants.FILE_STRING);
  }
  
}
