package com.tower.crtn.file.received.component;

import com.ibm.cloud.objectstorage.services.s3.AmazonS3;
import com.tower.crtn.file.received.constant.Constants;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import java.io.File;

@SpringBootTest
public class CosManagementFileTest {
  
  @InjectMocks
  private CosManagementFile cosManagementFile;
  
  @Mock
  private AmazonS3 amazonS3;
  
  private File file;
  
  private File file2;
  
  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
    
    file = Mockito.mock(File.class);
    
    file2 = new File("010101010101.txt");
    
  }
  
  @Test
  public void uploadLargeObjectFileTestNotIsFile() {
    cosManagementFile.uploadLargeObjectFile(
        Constants.NAME_STRING, Constants.NAME_STRING, file);
  }
  
  @Test
  public void uploadLargeObjectFileTestItsFile() {
    cosManagementFile.uploadLargeObjectFile(
        Constants.NAME_STRING, Constants.NAME_STRING, file2);
  }
}
