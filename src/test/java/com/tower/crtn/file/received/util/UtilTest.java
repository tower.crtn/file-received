
package com.tower.crtn.file.received.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tower.crtn.file.received.mongo.entity.FileMongo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UtilTest {

  private FileMongo fileMongo;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);

    fileMongo = new FileMongo("dasd", "dasda", "dsad", 7, 4324, "dasda");
  }

  @Test
  public void getJson() {
    Assertions.assertNotNull(Util.getJson(fileMongo));
  }

  @Test
  public void getJsonException() {

    ObjectMapper obj = new ObjectMapper(null);

    Assertions.assertNotNull(Util.getJson(obj));
  }

}
