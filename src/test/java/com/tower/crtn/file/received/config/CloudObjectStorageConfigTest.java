package com.tower.crtn.file.received.config;

import com.tower.crtn.file.received.constant.Constants;
import com.tower.crtn.file.received.properties.CloudObjectStorageProperties;
import lombok.Setter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CloudObjectStorageConfigTest {
  
  @InjectMocks
  private CloudObjectStorageConfig cloudObjectStorageConfig;
  
  @Mock
  private CloudObjectStorageProperties cloudObjectStorageProperties;
  
  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
  }
  
  @Test
  public void makeClientTest() {
    Mockito.when(cloudObjectStorageProperties.getApiKey()).thenReturn(Constants.FILE_STRING);
    Mockito.when(cloudObjectStorageProperties.getServerInstanceId()).thenReturn(Constants.FILE_STRING);
    Mockito.when(cloudObjectStorageProperties.getEndpointUrl()).thenReturn(Constants.FILE_STRING);
    Mockito.when(cloudObjectStorageProperties.getLocation()).thenReturn(Constants.FILE_STRING);
    
    Assertions.assertNotNull(cloudObjectStorageConfig.makeClient());
  }
  
}
