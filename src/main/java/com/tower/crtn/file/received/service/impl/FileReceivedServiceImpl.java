package com.tower.crtn.file.received.service.impl;

import com.ibm.cloud.objectstorage.services.s3.transfer.Upload;
import com.tower.crtn.file.received.component.CosManagementFile;
import com.tower.crtn.file.received.component.KafkaProducer;
import com.tower.crtn.file.received.constant.Constants;
import com.tower.crtn.file.received.model.KafkaMessageSend;
import com.tower.crtn.file.received.mongo.entity.FileMongo;
import com.tower.crtn.file.received.mongo.repository.FileMongoRepository;
import com.tower.crtn.file.received.properties.CloudObjectStorageProperties;
import com.tower.crtn.file.received.properties.KafkaProperties;
import com.tower.crtn.file.received.service.FileReceivedService;
import com.tower.crtn.file.received.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * File received service implements.
 * 
 * @author Nova.
 */
@Slf4j
@Service
public class FileReceivedServiceImpl implements FileReceivedService {

  /** Cos Management File component injection. */
  @Autowired
  private CosManagementFile cosManagementFile;

  /** Cloud Object Storage properties. */
  @Autowired
  private CloudObjectStorageProperties cloudObjectStorageProperties;

  /** Kafka producer component. */
  @Autowired
  private KafkaProducer kafkaProducer;

  /** Kafka properties. */
  @Autowired
  private KafkaProperties kafkaProperties;

  /** File mongo repository. */
  @Autowired
  private FileMongoRepository fileMongoRepository;

  /**
   * {@inheritDoc}.
   */
  @Override

  public boolean receivedFile(MultipartFile multipartFile, String name) {
    boolean fileSend = false;

    try {
      File file = new File(name);
      file.createNewFile();

      OutputStream fileOutputStream = new FileOutputStream(file);
      fileOutputStream.write(multipartFile.getBytes());
      fileOutputStream.close();


      String itemName = Util.getItemNameFile(name);

      long fileSize = file.length();


      String uuidIdentifier = UUID.randomUUID().toString();
      List<String> contentFile = Util.getContentAsList(file);

      fileMongoRepository.save(new FileMongo(uuidIdentifier, itemName, itemName, contentFile.size(),
          fileSize, Constants.PENDIENTE_STATUS));

      KafkaMessageSend kafkaMessageSend = new KafkaMessageSend(uuidIdentifier, itemName, fileSize);
      kafkaProducer.send(kafkaProperties.getTopicFileReciver(), Util.getJson(kafkaMessageSend));

      fileSend = true;

//      file.delete();
    } catch (IOException ex) {
      log.error(ex.getMessage(), ex);
    } catch (Exception ex) {
      log.error(ex.getMessage(), ex);
    }

    return fileSend;
  }
}
