package com.tower.crtn.file.received.properties;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Kafka properties.
 * 
 * @author DAT.
 */
@Component
@EnableConfigurationProperties
@Getter
public class KafkaProperties {
  
  /** Received topic name. */
  @Value("${properties.kafka.cloud-kafka-topic}")
  private String topicFileReciver;
  
}
