package com.tower.crtn.file.received.controller;

import com.tower.crtn.file.received.constant.Constants;
import com.tower.crtn.file.received.constant.NumericAndCharacterConstants;
import com.tower.crtn.file.received.properties.ApiProperties;
import com.tower.crtn.file.received.service.FileReceivedService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * Upload file controller.
 * 
 * @author Nova.
 */
@Slf4j
@Controller
@RequestMapping("${properties.mvc.api.basePath}")
public class UploadFileController {
  
  /** File received injection. */
  @Autowired
  private FileReceivedService fileReceivedService;
  
  /** Api properties. */
  @Autowired
  private ApiProperties apiProperties;

  /**
   * Expose a http view for interaction file.
   * 
   * @return a html resource.
   */
  @GetMapping("${properties.mvc.api.basePath}")
  public String index() {
    return Constants.RESOURCE_INDEX;
  }

  /**
   * Start the upload file process.
   * 
   * @param file file as multipart file.
   * @param name file name.
   * @return a message that indicate if the file has been uploaded. 
   */
  @PostMapping("${properties.mvc.api.specificPaths.uploadFile}")
  public ResponseEntity<String> uploadFile(@RequestParam(Constants.FILE_STRING) MultipartFile file,
      @RequestParam(Constants.NAME_STRING) String name) {

    Date dateInicial = new Date();
    
    String message = fileReceivedService.receivedFile(file, name) ? apiProperties.getResponseOk()
        : apiProperties.getResponseBad();
    
    message = apiProperties.getBaseResponse() + NumericAndCharacterConstants.SPACE_EMPTY + message;
    log.info(message);
    
    Date dateFinal = new Date();
    
    log.info("\nTiempo inicial: " + dateInicial + "\nTiempo final: " + dateFinal + "\n");
    
    return new ResponseEntity<>(message, HttpStatus.OK);
  }
}
