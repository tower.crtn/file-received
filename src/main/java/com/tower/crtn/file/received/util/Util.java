package com.tower.crtn.file.received.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tower.crtn.file.received.constant.Constants;
import com.tower.crtn.file.received.constant.NumericAndCharacterConstants;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Utility methods class.
 * 
 * @author Nova.
 */
@Slf4j
public class Util {

  /**
   * Write a object and attributes as String with Json structure.
   * 
   * @param object any object.
   * @return a String as a Json.
   */
  public static String getJson(Object object) {
    String json = null;
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      json = objectMapper.writeValueAsString(object);
    } catch (Exception ex) {
      log.error(ex.getMessage(), ex);
    }
    return json;
  }

  /**
   * Make a new format for file name.
   * 
   * @param fileName original file name.
   * @return new file name.
   */
  public static String getItemNameFile(String fileName) {
    Date date = new Date();
    SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT_DDMMYYYYHHMMSS);

    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(format.format(date)).append(NumericAndCharacterConstants.HYPHEN_SYMBOL)
        .append(fileName);

    return stringBuilder.toString();
  }

  /**
   * Read a file and add this lines in a List.
   * 
   * @param file file.
   * @return a List with content.
   */
  public static List<String> getContentAsList(File file) {
    List<String> list = new ArrayList<>();

    try {
      FileReader fileReader = new FileReader(file);
      BufferedReader bufferedReader = new BufferedReader(fileReader);

      String line = NumericAndCharacterConstants.EMPTY_STRING;
      while ((line = bufferedReader.readLine()) != null) {
        list.add(line);
      }
      bufferedReader.close();
      fileReader.close();

    } catch (FileNotFoundException ex) {
      log.error(ex.getMessage(), ex);
    } catch (Exception ex) {
      log.error(ex.getMessage(), ex);
    }

    return list;
  }

}
