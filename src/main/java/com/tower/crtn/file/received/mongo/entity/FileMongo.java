package com.tower.crtn.file.received.mongo.entity;

import com.tower.crtn.file.received.constant.MongoConstants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * File mongo collection structure.
 * 
 * @author DAT.
 */
@AllArgsConstructor
@Data
@Document(collection = MongoConstants.MONGO_FILE_COLLECTION)
@NoArgsConstructor
public class FileMongo {
  
  /** Uuid file. */
  private String uuid;
  
  /** File name. */
  private String fileName;
  
  /** Path in Cloud Object Storage. */
  private String pathCos;
  
  /** Transactions number. */
  private long transactionNumber;
  
  /** Size file. */
  private long sizeFile;
  
  /** File status. */
  private String fileStatus;
  
}
