package com.tower.crtn.file.received.constant;

/**
 * Numeric and Character constants.
 * 
 * @author DAT.
 */
public class NumericAndCharacterConstants {
  
  /** Value int 0. */
  public static final int VALUE_INT_0 = 0;
  /** Value int 1. */
  public static final int VALUE_INT_1 = 1;
  /** Value int 2. */
  public static final int VALUE_INT_2 = 2;
  /** Value int 3. */
  public static final int VALUE_INT_3 = 3;
  /** Value int 4. */
  public static final int VALUE_INT_4 = 4;
  
  /** Value int 100. */
  public static final int VALUE_INT_100 = 100;
  /** Value int 500. */
  public static final int VALUE_INT_500 = 500;
  /** Value int 1024. */
  public static final int VALUE_INT_1024 = 1024;
  /** Value long 5242880, represents a 5 MB. */
  public static final long VALUE_LONG_5242880_IS_A_5_MB = 5242880;
  /** Value long 10485760, represents a 10 MB. */
  public static final long VALUE_LONG_10485760_IS_A_10_MB = 10485760;
  
  
  /** Hyphen symbol. */
  public static final String HYPHEN_SYMBOL = "-";
  /** Empty value. */
  public static final String EMPTY_STRING = "";
  /** New line character. */
  public static final String NEW_LINE = "\n";
  /** Space value symbol. */
  public static final String SPACE_EMPTY = " ";
  
}
