package com.tower.crtn.file.received.constant;

/**
 * Content the constants for this Service.
 * 
 * @author DAT.
 */
public class Constants {

  /** Resource index for upload file. */
  public static final String RESOURCE_INDEX = "index";


  /** File as attribute for upload file. */
  public static final String FILE_SAVE_STRING = "file-save-";
  /** File as attribute for upload file. */
  public static final String FILE_STRING = "file";
  /** File name as attribute for upload file. */
  public static final String NAME_STRING = "name";


  /** Content-Type for COS application. */
  public static final String CONTENT_TYPE_IN_COS = "application/x-java-serialized-object";


  /** Date format for file name. */
  public static final String DATE_FORMAT_DDMMYYYYHHMMSS = "ddMMyyyyHHmmss";

  /** Pending status. */
  public static final String PENDIENTE_STATUS = "Pendiente";
}
