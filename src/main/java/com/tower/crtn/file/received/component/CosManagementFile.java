package com.tower.crtn.file.received.component;

import com.ibm.cloud.objectstorage.AmazonClientException;
import com.ibm.cloud.objectstorage.services.s3.AmazonS3;
import com.ibm.cloud.objectstorage.services.s3.transfer.TransferManager;
import com.ibm.cloud.objectstorage.services.s3.transfer.TransferManagerBuilder;
import com.ibm.cloud.objectstorage.services.s3.transfer.Upload;
import com.tower.crtn.file.received.constant.NumericAndCharacterConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * Cloud Object Storage management.
 * 
 * @author DAT.
 */
@Slf4j
@Component
public class CosManagementFile {

  /** Amazon S3 instance. */
  @Autowired
  private AmazonS3 amazonS3;


  /**
   * Upload file.
   * 
   * @param bucketName bucket name.
   * @param itemName file name.
   * @param file file.
   * @return return a upload reference.
   */
  public Upload uploadLargeObjectFile(String bucketName, String itemName, File file) {
    Upload upload = null;
    
    if (file.isFile()) {
      TransferManager transferManager = TransferManagerBuilder.standard().withS3Client(amazonS3)
          .withMinimumUploadPartSize(NumericAndCharacterConstants.VALUE_LONG_5242880_IS_A_5_MB)
          .withMultipartCopyPartSize(NumericAndCharacterConstants.VALUE_LONG_5242880_IS_A_5_MB)
          .build();

      try {
        upload = transferManager.upload(bucketName, itemName, file);
      } catch (AmazonClientException ex) {
        log.error(ex.getMessage(), ex);
      } 
    }
    return upload;
  }
}
