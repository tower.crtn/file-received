package com.tower.crtn.file.received;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring boot main class.
 * 
 * @author DAT.
 */
@SpringBootApplication
public class FileReceivedApplication {

  /**
   * Main method.
   * 
   * @param args arguments.
   */
  public static void main(String[] args) {
    SpringApplication.run(FileReceivedApplication.class);
  }
}
