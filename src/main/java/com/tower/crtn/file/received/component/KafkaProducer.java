package com.tower.crtn.file.received.component;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * Kafka producer component.
 * 
 * @author DAT.
 */
@Component
public class KafkaProducer {
  
  /** KafkaTemplate object. */
  private final KafkaTemplate<String, String> kafkaTemplate;
  
  /**
   * KafkaProducer component.
   * 
   * @param kafkaTemplate kafka template.
   */
  public KafkaProducer(KafkaTemplate<String, String> kafkaTemplate) {
    this.kafkaTemplate = kafkaTemplate;
  }
  
  /**
   * Send method.
   * 
   * @param topic topic name.
   * @param message message to send.
   */
  public void send(String topic, String message) {
    kafkaTemplate.send(topic, message);
  }
  
}
