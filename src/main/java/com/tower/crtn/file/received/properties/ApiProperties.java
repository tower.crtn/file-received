package com.tower.crtn.file.received.properties;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Api properties.
 * 
 * @author DAT.
 */
@Component
@EnableConfigurationProperties
@Getter
public class ApiProperties {
  
  /** Message base response. */
  @Value("${properties.mvc.message.baseResponse}")
  private String baseResponse;
  
  /** Message for response ok. */
  @Value("${properties.mvc.message.responseOk}")
  private String responseOk;
  
  /** Message for response error. */
  @Value("${properties.mvc.message.responseBad}")
  private String responseBad;
}
