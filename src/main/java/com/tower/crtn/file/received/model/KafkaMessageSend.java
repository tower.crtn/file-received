package com.tower.crtn.file.received.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Kafka Message Send value for parsing.
 * 
 * @author DAT.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class KafkaMessageSend {

  /** Uuid file identifier. */
  private String uuidFileIdentifier;
  /** File name. */
  private String fileName;
  /** File size. */
  private long fileSize;

}
