package com.tower.crtn.file.received.mongo.repository;

import com.tower.crtn.file.received.mongo.entity.FileMongo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * File Mongo repository.
 * 
 * @author DAT.
 */
@Repository
public interface FileMongoRepository extends MongoRepository<FileMongo, String> {
  
}
