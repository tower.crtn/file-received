package com.tower.crtn.file.received.constant;

/**
 * Mongo constants.
 * 
 * @author DAT.
 */
public class MongoConstants {
  
  /** Collection 'File' name. */
  public static final String MONGO_FILE_COLLECTION = "File";

}
