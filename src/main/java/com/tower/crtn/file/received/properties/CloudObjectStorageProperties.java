package com.tower.crtn.file.received.properties;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Cloud Object Storage properties.
 * 
 * @author DAT.
 */
@Component
@EnableConfigurationProperties
@Getter
public class CloudObjectStorageProperties {
  
  /** Property with bucket name value. */
  @Value("${properties.cos.bucketName}")
  private String bucketName;
  
  /** Property with api key value. */
  @Value("${properties.cos.apiKey}")
  private String apiKey;
  
  /** Property with service instance id value. */
  @Value("${properties.cos.serverInstanceId}")
  private String serverInstanceId;
  
  /** Property with end point url id value. */
  @Value("${properties.cos.endpointUrl}")
  private String endpointUrl;
  
  /** Property with location value. */
  @Value("${properties.cos.location}")
  private String location;
}
