package com.tower.crtn.file.received.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * File received service.
 * 
 * @author DAT.
 */
public interface FileReceivedService {

  /**
   * Received file.
   * 
   * @param multipartFile multipart file.
   * @param name file name.
   * @return a flag that indicates if the file is upload or not.
   */
  boolean receivedFile(MultipartFile multipartFile, String name);
  
}
